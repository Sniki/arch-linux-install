#!/bin/bash

ln -sf /usr/share/zoneinfo/Europe/Tirane /etc/localtime
hwclock --systohc
sed -i '178s/.//' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "arch" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 arch.localdomain arch" >> /etc/hosts
echo root:password | chpasswd

# Grub Bootloader Install

grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=Linux 

# Create Grub configuration file

grub-mkconfig -o /boot/grub/grub.cfg


# important packages

pacman -S grub efibootmgr os-prober xorg xorg-xinit xorg-xbacklight pipewire pipewire-pulse pipewire-jack pipewire-alsa alsa-utils networkmanager modemmanager bluez bluez-utils blueman cups hplip firewalld acpi base-devel wget git curl btrfs-progs exfatprogs dosfstools udftools xfsprogs mtools fish p7zip unrar unzip zip neofetch bash-completion pcmanfm sddm nitrogen lxappearance lxsession alacritty picom dunst xdg-user-dirs firewalld ttf-ubuntu-font-family ttf-dejavu ttf-bitstream-vera ttf-roboto ttf-hack ttf-font-awesome ttf-joypixels terminus-font 

# Enable Services

systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable cups.service
systemctl enable firewalld

# Create user with fish shell

useradd -m -g users -G wheel -s /usr/bin/fish sniki
echo sniki:password | chpasswd

# Add user to sudoers

echo "sniki ALL=(ALL) ALL" >> /etc/sudoers.d/sniki


printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"




