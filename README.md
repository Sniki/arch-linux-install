# Arch Linux Install
(Simple documentation of the packages and stuff that i use when doing a clean arch linux install so i don't forget any of the packages).

## Packages

### Xorg
xorg xorg-xinit xorg-xbacklight xorg-fonts-misc

### Drivers
xf86-video-intel mesa libva-intel-driver vulkan-intel vulkan-icd-loader
nvidia nvidia-utils nvidia-settings
pipewire pipewire-jack pipewire-pulse pipewire-alsa alsa-utils
bluez bluez-utils blueman
networkmanager network-manager-applet modemmanager
acpid acpi 
btrfs-progs xfsprogs exfatprogs dosfstools udftools gvfs udiskie2 udiskie 

### Archiving & Compression Tools
p7zip unrar unzip zip

### Developer tools
base-devel wget git curl

### Utilities
fish starship neofetch xdg-user-dirs picom nitrogen lxappearance alacritty arc-gtk-theme arc-icon-theme firefox dunst viewnior pcmanfm cups hplip geany sddm lxsession

### Fonts
ttf-ubuntu-font-family ttf-bitstream-vera ttf-dejavu ttf-roboto noto-fonts ttf-joypixels ttf-font-awesome ttf-hack

## Window Managers

### Qtile
qtile python-psutil python-dbus-next python-iwlib pywlroots

## Enable System Services
systemctl enable NetworkManager ModemManager bluetooth cups acpid sddm
